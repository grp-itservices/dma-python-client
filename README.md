DMA API client for Python
=========================

Development installation
------------------------

Clone the project, install build tools:
```
$ git clone git@git.embl.de:grp-itservices/dma-python-client.git
$ cd dma-python-client
$ pip install poetry
```

Install package dependencies:
```
$ poetry install
```

Run functional tests:
```
$ docker run -d -p 4010:4010 sourcecode.embl.de:5050/dma/utils/dma-mock-api:latest
$ poetry run pytest -v
```

Regular installation
--------------------

Install package with `pip`:
```
$ pip install dma-client --extra-index-url https://git.embl.de/api/v4/projects/4393/packages/pypi/simple
```
or add it to your `requirements.txt` as follows:
```
--extra-index-url https://git.embl.de/api/v4/projects/4393/packages/pypi/simple
dma-client==0.0.1
```

Usage examples
--------------

* Initialize client:
  ```python
  import os
  from dma_client import RESTClient

  # NB: no hardcoded credentials!
  username, password = os.getenv('DMA_API_USERNAME'), os.getenv('DMA_API_PASSWORD')

  client = RESTClient('https://dma.test.embl.de/api/v1')
  ```

* Authenticate in the default `USER` scope:
  ```python
  client.authenticate(username, password)
  ```

  You can also pass the `scope` argument explicitly:
  ```python
  client.authenticate(username, password, scope='FOO')
  ```

* Authenticate with static authentication token:
  ```python
  client.set_auth_token('FOOSECRET123')
  ```

  Multiple tokens can be passed via a JSON file:
  ```json
  {
      "auth_tokens": [{
              "url": "https://dma.test.embl.de/api/v1",
              "token": "token-test"
          }, {
              "url": "https://dma.embl.de/api/v1",
              "token": "token-prod"
          }
      ]
  }
  ```

  ```python
  client = RESTClient('https://dma.test.embl.de/api/v1', config_path ='./config.json')
  ```

* Set a different API URL:
  ```python
  client.set_url('https://dma.test.embl.de/api/v1')
  ```

* Log out:
  ```python
  client.logout()
  ```

* List data objects associated with the user:
  ```python
  client.list_data()
  ```

* Register a new collection, add data to it:
  ```python
  collection = client.register_collection(name='test_collection', comment='My files')
  data = client.register_data(
      path='/foo/baz',
      name='test_data',
      collection_id=collection.id,
      comment='My precious files',
  )
  ```

* Request to archive data:
  ```python
  client.request_archive(
      data_id=data.data_id,
      budget_number=12345,
      delete_data=False,
      comment='No longer needed',
  )
  ```

* Request a handover of data:
  ```python
  client.request_handover(
      data_id=12345,
      copy_to='/g/foo/bar',
      set_owner='new_owner',
      comment='test handover from client',
      delete_original=False,
      set_name='new_data_name',
      limit_to='/g/foo/bar/baz'
  )
  ```

* Request a data pull from a remote machine:
  ```python
  client.request_pull(
      target_path='/foo/bar',
      original_path='/remote/machine/path',
      original_machine='fooMachine',
      collection_id=1234,
      comment='comment',
      name='data_name'
  )
  ```

* Request a data pullover from a remote machine:
  ```python
  client.request_pull(
      target_path='/foo/bar',
      original_path='/remote/machine/path',
      original_machine='fooMachine',
      set_owner='fooUser',
      comment='comment',
      name='data_name'
  )
  ```

* List the requested actions, including their current status:
  ```python
  client.list_actions()
  ```

* Get the status of a given data:
  ```python
  client.get_data_status(data.data_id)
  ```
  
* Get the list of volumes for the logged in user:
  ```python
  client.list_volumes()
  ```
