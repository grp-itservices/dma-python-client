"""
    The client's root module.
"""
from .exceptions import (
    ClientException,
    ClientSessionError,
    ClientUnathorized,
    ClientActionError,
    ClientConfigError,
)
from .client import RESTClient
from .models import Data, Collection, Action

__all__ = (
    'RESTClient',
    'models',
    'ClientException',
    'ClientSessionError',
    'ClientUnathorized',
    'ClientActionError',
    'ClientConfigError',
    'Data',
    'Collection',
    'Action',
)
