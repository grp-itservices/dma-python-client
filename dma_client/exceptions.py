"""
    Client exceptions.
"""

class ClientException(Exception):
    """
    Base client exception.
    """

class ClientSessionError(ClientException):
    """
    Missing or broken client session.
    """

class ClientUnathorized(ClientException):
    """
    Client is not authenticated.
    """

class ClientActionError(ClientException):
    """
    Error while executing an action.
    """
class ClientConfigError(ClientException):
    """
    Error while load client config.
    """
