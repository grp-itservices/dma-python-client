"""
    Various data models.
"""
from datetime import datetime
from pathlib import PosixPath
from typing import Optional, List, Union

from pydantic import BaseModel, Field, Extra, validator  # pylint:disable=E0611


class DMABaseModel(BaseModel): # pylint:disable=R0903
    """
    Base model used to apply config to all DMA model classes
    """
    class Config:  # pylint:disable=R0903
        """
        Model settings.
        """
        allow_mutation = False
        extra = Extra.ignore


class BasicDataStatus(DMABaseModel):  # pylint:disable=R0903
    """
    DMA Data Basic Status Information class.
    Includes a quick overview of the data status (e.g. is a data archived true/false)
    """
    deleted: bool
    archived: bool
    shared: bool
    has_copy: bool = Field(..., alias='hasCopy')
    is_copy: bool = Field(..., alias='isCopy')
    has_extraction: bool = Field(..., alias='hasExtraction')
    is_extraction: bool = Field(..., alias='isExtraction')


class Data(DMABaseModel):  # pylint:disable=R0903
    """
    DMA Data class.
    """
    data_id: int = Field(..., alias='id')
    path: PosixPath
    name: Optional[str]
    owner: str
    deleted: bool
    comment: Optional[str]
    created: datetime
    data_type: str = Field(..., alias='type')
    collection_id: Optional[int] = Field(..., alias='collectionId')
    status: Optional[BasicDataStatus]


class MetaData(DMABaseModel):  # pylint:disable=R0903
    """
    DMA Meta Data class.
    """
    id: int
    data_id: int = Field(..., alias='dataId')
    metadata_type: dict = Field(..., alias='metadataType')
    value: str
    metadata_type_values: List[dict] = Field(..., alias='metadataTypeValues')


class Share(DMABaseModel):  # pylint:disable=R0903
    """
    DMA Share class.
    """
    data_id: int = Field(..., alias='dataId')
    comment: str
    timestamp: datetime
    shared_with: str = Field(..., alias='sharedWith')
    shared_by: str = Field(..., alias='sharedBy')
    write_access: bool = Field(..., alias='writeAccess')


class Archive(DMABaseModel):  # pylint:disable=R0903
    """
    DMA Archive class.
    """
    archive_id: int = Field(..., alias='id')
    data_id: int = Field(..., alias='dataId')
    comment: str
    budget_number: int = Field(..., alias='budgetNumber')
    timestamp: datetime
    log_file_url: str = Field(..., alias='logFileUrl')
    size: int


class Copy(DMABaseModel):  # pylint:disable=R0903
    """
    DMA Copy class.
    """
    source_data_id: int = Field(..., alias='sourceDataId')
    source_location: str = Field(..., alias='sourceLocation')
    target_data_id: int = Field(..., alias='targetDataId')
    target_location: str = Field(..., alias='targetLocation')
    delete_original: bool = Field(..., alias='deleteOriginal')
    requested_by: str = Field(..., alias='requestedBy')
    copy_owner: str = Field(..., alias='setOwner')
    is_partial_copy: bool = Field(..., alias='partialCopy')
    comment: Optional[str]
    timestamp: datetime


class DataStatus(DMABaseModel):  # pylint:disable=R0903
    """
    DMA Data Status class.
    """
    data_id: int = Field(..., alias='datasetId')
    archives: List[Archive]
    available: bool
    children: List[Data]
    copies: List[Copy]
    copy_of: Optional[Data] = Field(..., alias='copyOf')
    deleted: bool
    parent: Optional[Data]
    has_pending_actions: bool = Field(..., alias='pendingActions')
    removable: bool
    shares: List[Share]


class Collection(DMABaseModel): # pylint:disable=R0903
    """
    DMA Collection Object.
    """
    id: int
    name: str
    owner: str
    created_at: datetime = Field(..., alias='createdAt')
    description: Optional[str]


class Action(DMABaseModel): # pylint:disable=R0903
    """
    Action Object.
    """

    class Config:  # pylint:disable=R0903
        """
        Model settings.
        """
        allow_population_by_field_name = True

    id: int
    status: str
    timestamp: datetime
    result: Optional[str]
    result_data: Union[Archive, Share, dict] = Field(None, alias='resultData')
    error: Optional[dict]
    requester: str = Field(..., alias='requesterId')
    requester_scope: Optional[str] = Field(None, alias='requesterScope')
    type: str
    entity_type: str = Field(..., alias='entityType')
    entity_id: int = Field(..., alias='entityId')


class Volume(DMABaseModel):  # pylint:disable=R0903
    """
    DMA Volume class.
    """
    name: str = Field(..., alias='volumeName')
    path: PosixPath = Field(..., alias='volumePath')


class Machine(DMABaseModel):  # pylint:disable=R0903
    """
    DMA Machine class.
    """
    id: int
    name: str
    user: str
    ip_address: str = Field(..., alias='ipAddress')
    ssh_port: int = Field(..., alias='sshPort')
    label: Optional[str]
    responsible_person: Optional[str] = Field(None, alias='responsiblePerson')
    note: Optional[str]
