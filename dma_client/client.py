"""
    Python client for the DMA application.
"""
import json
import logging
from pathlib import Path, PosixPath
from typing import Callable, List, Optional, Union
from urllib.parse import urljoin

import decorator
import requests
import sqlitedict  # type: ignore

from . import models
from .exceptions import (
    ClientException,
    ClientSessionError,
    ClientUnathorized,
    ClientActionError,
    ClientConfigError,
)


@decorator.decorator
def login_required(method: Callable, *args, **kwargs) -> Callable:
    """
    Check if the session is initialized and the user is logged in.
    """
    self, *_ = args
    if not self.session:
        raise ClientUnathorized('Please authenticate.')
    return method(*args, **kwargs)


class APISession(requests.Session):
    """
    Request's session with a fixed URL base and static Authorization header.
    """
    def __init__(self, prefix: str, bearer_token: str, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._prefix: str = prefix.rstrip('/') + '/'
        self._bearer_token: str = bearer_token
        # NB: ensure the attributes can be pickled
        self.__attrs__.extend(('_prefix', '_bearer_token'))

    def request(self, method, url, *args, **kwargs):
        url = urljoin(self._prefix, url.lstrip('/'))
        headers = {
            'Authorization': f'Bearer { self._bearer_token }',
            **kwargs.pop('headers', {}),
        }
        return super().request(method, url, *args, headers=headers, **kwargs)


class RESTClient:
    """
    Client for the high-level DMA's REST API.
    """

    def __init__(
        self,
        api_url: str,
        cache_path: Optional[Union[Path, str]] = None,
        config_path: Optional[Union[Path, str]] = None
    ) -> None:
        self.api_url = api_url.rstrip('/')
        self.cache: dict = {}
        if cache_path:
            path = str(Path(cache_path).resolve())
            logging.info('Using cache at "%s".', path)
            self.cache = sqlitedict.SqliteDict(path, autocommit=True)
        if config_path:
            self._load_config(config_path)

    def _load_config(self, config_path):
        """
        Load multiple tokens from JSON file.
        """
        path = Path(config_path).resolve()
        logging.info('Using config at "%s".', path)
        try:
            config = json.loads(path.read_text(encoding='utf8'))
            for token in config.get('auth_tokens', ()):
                self.set_url(token['url'])
                self.set_auth_token(token['token'])
                logging.info('Set static token for "%(url)s".', token)
        # AttributeError
        except FileNotFoundError:
            # pylint:disable=raise-missing-from
            raise ClientConfigError(f'"{ path }": no such file.')
        except (AttributeError, KeyError, TypeError, json.JSONDecodeError):
            # pylint:disable=raise-missing-from
            raise ClientConfigError(f'Invalid JSON at "{ path }.')

    @property
    def session(self) -> APISession:
        """
        Return the current session instance.
        """
        key = f'session:{ self.api_url }'
        if key not in self.cache:
            raise ClientSessionError(f'No session for {self.api_url}')
        return self.cache[key]

    @session.setter
    def session(self, session: Union[APISession, None]) -> None:
        """
        Cache a new session instance.
        """
        self.cache[f'session:{ self.api_url }'] = session

    def set_url(self, api_url: str) -> None:
        """
        Set a new API URL, attempt to recover previously saved session.
        """
        self.api_url = api_url

    def authenticate(  # pylint:disable=R0913
        self,
        username: str,
        password: str,
        scope: Optional[str] = 'USER',
        application: Optional[str] = None,
        permanent: bool = False,
    ):
        """
        Authenticate with the remote API, store access token.
        """
        auth = {
            'username': username,
            'password': password,
            'scope': scope,
            'application': application,
        }
        endpoint = 'loginPermanent' if permanent else 'login'
        url = f'{ self.api_url }/{ endpoint }'
        try:
            resp = requests.post(url, data=auth)
        except Exception as exc:
            raise ClientException from exc
        if resp.status_code != 200:
            raise ClientUnathorized('Could not authenticate.')
        self.set_auth_token(resp.text)

    def set_auth_token(self, auth_token: str) -> None:
        """
        Set authentication token
        """
        self.session = APISession(self.api_url, auth_token)

    def logout(self) -> None:
        """
        Remove cached authentication.
        """
        try:
            del self.cache[f'session:{ self.api_url }']
        except KeyError:
            # pylint:disable=raise-missing-from
            raise ClientSessionError(f'No session for "{ self.api_url }".')

    @login_required
    def list_data(  # pylint:disable=R0913
        self,
        name_contains: Optional[str] = None,
        path_contains: Optional[str] = None,
        comment_contains: Optional[str] = None,
        deleted: Optional[bool] = None,
        data_type: Optional[str] = None,
        additional_info: Optional[bool] = None
    ) -> List[models.Data]:
        """
        List all the data available to the user.
        """
        params = {
            'nameContains': name_contains,
            'pathContains': path_contains,
            'commentContains': comment_contains,
            'deleted': deleted,
            'type': data_type,
            'additionalInfo': additional_info
        }
        resp = self.session.get('/data', params=params)  # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to list data: HTTP{ resp.status_code } "{ resp.text }".'
            )
        return [models.Data(**kw) for kw in resp.json()]

    @login_required
    def get_data(self, data_id: int) -> models.Data:
        """
        Get the data with the given id.
        """
        resp = self.session.get(f'/data/{data_id}')  # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to get data: HTTP{resp.status_code} "{resp.text}".'
            )
        return models.Data(**resp.json())

    @login_required
    def get_data_status(self, data_id: int) -> models.DataStatus:
        """
        Get the data status for the data with the given id.
        """
        resp = self.session.get(f'/data/{data_id}/status')  # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to get data status: HTTP{resp.status_code} "{resp.text}".'
            )
        return models.DataStatus(**resp.json())

    @login_required
    def get_metadata(self, data_id: int) -> List[models.MetaData]:
        """
        Get the metadata for the data with the given id.
        """
        resp = self.session.get(f'/data/{data_id}/metadata')
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to get metadata: HTTP{resp.status_code} '
                f'"{resp.text}".'
            )
        return [models.MetaData(**item) for item in resp.json()]

    @login_required
    def delete_metadata(self, data_id: int, metadata_id: int) -> None:
        """
        Delete the metadata with the given id.
        """
        resp = self.session.delete(f'/data/{data_id}/metadata/{metadata_id}')
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to delete metadata: HTTP{resp.status_code} '
                f'"{resp.text}".'
            )

    @login_required
    def list_actions(self,
         entity_id: Optional[int] = None,
         entity_type: Optional[str] = None,
         action_type: Optional[str] = None,
         action_status: Optional[str] = None
     ) -> List[models.Action]:
        """
        List all the actions requested by the user - including the current Action status.
        """
        kwargs = {
            'entityId': entity_id,
            'entityType': entity_type,
            'type': action_type,
            'status': action_status,
        }
        params = {k: v for k, v in kwargs.items() if v}
        resp = self.session.get('/actions', params=params) # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to list actions: HTTP{ resp.status_code } "{ resp.text }".'
            )
        return [models.Action(**kw) for kw in resp.json()]

    @login_required
    def register_data(  # pylint:disable=R0913
        self,
        path: PosixPath,
        comment: Optional[str] = None,
        name: Optional[str] = None,
        collection_id: Optional[int] = None,
        endpoint: Optional[str] = '/data'
    ) -> models.Data:
        """
        Register a new data item.
        """
        kwargs = {
            'path': path,
            'comment': comment,
            'name': name,
            'collectionId': collection_id,
        }
        payload = {k: v for k, v in kwargs.items() if v}
        resp = self.session.post(endpoint, json=payload) # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to register data: HTTP{ resp.status_code } "{ resp.text }".'
            )
        return models.Data(**resp.json())

    @login_required
    def register_labid_data(
        self,
        path: PosixPath,
        comment: Optional[str] = None,
        name: Optional[str] = None,
        collection_id: Optional[int] = None
    ) -> models.Data:
        """
        Register a new data item which is to be owned by user stocks using
        the `/data/labid` API endpoint. Only accessible to users in the
        GBCS scope and only accepts valid stocks paths.
        """
        return self.register_data(path, comment, name, collection_id, '/data/labid')

    @login_required
    def unregister_data(self, data_id: int) -> None:
        """
        Unregister a dataset.
        """
        resp = self.session.delete(f'/data/{data_id}')  # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to unregister data: HTTP{ resp.status_code } "{ resp.text }".'
            )

    @login_required
    def list_collections(self,
                        name: Optional[str] = None,
                        name_contains: Optional[str] = None,
                        description: Optional[str] = None,
                        description_contains: Optional[str] = None) -> List[models.Collection]:
        """
        Get collection filtered by payload values.

        Args:
            name (str): Matches the exact name of the collection
            name_contains (str): Compares if name_contains is in the collection name
            description (str): Matches the exact description of the collection
            description_contains (str): Compares if description_contains is in the description Name
        """
        params = {
                  'nameContains': name_contains,
                  'descriptionContains': description_contains
                 }
        resp = self.session.get('/collections', params=params) # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to list collections: HTTP{ resp.status_code } "{ resp.text }".'
            )
        collections = [models.Collection(**kw) for kw in resp.json()]

        # Additional filtering for exact matches
        if name:
            collections = [col for col in collections if col.name == name]
        if description:
            collections = [col for col in collections if col.description == description]
        return collections

    @login_required
    def get_collection(self, collection_id: int) -> models.Collection:
        """
        Get collection per id.
        """
        resp = self.session.get(f'/collections/{collection_id}') # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to get collection: HTTP{ resp.status_code } "{ resp.text }".'
            )
        return models.Collection(**resp.json())

    @login_required
    def register_collection(self, name: str, comment: str) -> models.Collection:
        """
        Register a new collection.
        """
        payload = {
            'name': name,
            'comment': comment
        }
        resp = self.session.post('/collections', json=payload) # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to register collection: HTTP{ resp.status_code } "{ resp.text }".'
            )
        return models.Collection(**resp.json())

    @login_required
    def unregister_collection(self, collection_id: int) -> None:
        """
        Unregister a collection.
        """
        resp = self.session.delete(f'/collections/{collection_id}')  # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to unregister collection: HTTP{ resp.status_code } "{ resp.text }".'
            )

    @login_required
    def request_pull(  # pylint:disable=R0913
        self,
        target_path: PosixPath,
        original_path: PosixPath,
        original_machine: str,
        collection_id: int,
        comment: Optional[str] = None,
        name: Optional[str] = None,
        callback_url: Optional[str] = None
    ) -> models.Data:
        """
        Request data pull.
        """
        kwargs = {
            'path': target_path,
            'comment': comment,
            'name': name,
            'collectionId': collection_id,
            'originalPath': original_path,
            'originalMachine': original_machine,
            'callbackUrl': callback_url,
        }
        payload = {k: v for k, v in kwargs.items() if v}
        resp = self.session.post('/data/pull', json=payload)  # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to request pull: HTTP{ resp.status_code } "{ resp.text }".'
            )
        return models.Data(**resp.json())

    @login_required
    def request_pullover(  # pylint:disable=R0913
        self,
        target_path: PosixPath,
        original_path: PosixPath,
        original_machine: str,
        set_owner: str,
        comment: Optional[str] = None,
        name: Optional[str] = None,
        collection_id: Optional[int] = None,
        callback_url: Optional[str] = None
    ) -> models.Data:
        """
        Request data pullover.
        """
        kwargs = {
            'path': str(target_path),
            'comment': comment,
            'name': name,
            'collectionId': collection_id,
            'originalPath': str(original_path),
            'originalMachine': original_machine,
            'handoverTo': set_owner,
            'callbackUrl': callback_url
        }
        payload = {k: v for k, v in kwargs.items() if v}
        resp = self.session.post('/data/pullover', json=payload)  # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to request pullover: HTTP{ resp.status_code } "{ resp.text }".'
            )
        return models.Data(**resp.json())

    @login_required
    def request_archive(  # pylint:disable=R0913
        self,
        data_id: int,
        budget_number: str,
        delete_data: bool,
        comment: str,
        callback_url: Optional[str] = None
    ) -> models.Action:
        """
        Request to archive the specified data
        """
        payload = {
            'budgetNumber': budget_number,
            'deleteData': delete_data,
            'comment': comment,
            'callbackUrl': callback_url
        }
        resp = self.session.post(f'/data/{data_id}/archive', json=payload)  # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to request archive: HTTP{ resp.status_code } "{ resp.text }".'
            )
        return models.Action(**resp.json())

    @login_required
    def request_delete(
        self,
        data_id: int,
    ) -> models.Action:
        """
        Request a delete action of the specified data
        """
        resp = self.session.post(f'/data/{data_id}/delete')  # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to request delete: HTTP{resp.status_code}: "{resp.text}".'
            )
        return models.Action(**resp.json())

    @login_required
    def request_share(  # pylint:disable=R0913
        self,
        data_id: int,
        share_with: str,
        write_access: bool,
        comment: str,
        callback_url: Optional[str] = None
    ) -> models.Action:
        """
        Request to share the specified data
        """
        payload = {
            'userId': share_with,
            'writeAccess': write_access,
            'comment': comment,
            'callbackUrl': callback_url
        }
        resp = self.session.post(f'/data/{data_id}/share', json=payload)  # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to request share: HTTP{ resp.status_code } "{ resp.text }".'
            )
        return models.Action(**resp.json())

    @login_required
    def get_action(self, action_id: int) -> models.Action:
        """
        Get the action with the given id.
        """
        resp = self.session.get(f'/actions/{action_id}')
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to get action: HTTP{resp.status_code} "{resp.text}".'
            )
        return models.Action(**resp.json())

    @login_required
    def action_status(self, action_id: int) -> str:
        """
        Return the current action status.
        """
        resp = self.session.get(f'/actions/{ action_id }/status')
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to retrieve action status: HTTP{ resp.status_code } "{ resp.text }".'
            )
        return resp.text

    @login_required
    def request_restore(
        self,
        data_id: int,
        archive_id: int,
        restore_to: Optional[PosixPath] = None,
        callback_url: Optional[str] = None
    ) -> models.Action:
        """
        Request a Restore of an archive for the specified data.
        """
        payload = {
            ('archiveId', archive_id),
            ('restoreTo', restore_to) if restore_to else None,
            ('callbackUrl', callback_url) if callback_url else None,
        }
        resp = self.session.post(
            f'/data/{data_id}/restore',
            json=dict(filter(bool, payload))  # type: ignore
        )
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to request restore: HTTP{ resp.status_code } "{ resp.text }".'
            )
        return models.Action(**resp.json())

    @login_required
    def request_handover_labid(  # pylint:disable=R0913
            self,
            data_id: int,
            copy_to: PosixPath,
            comment: str,
            delete_original: bool,
            set_name: Optional[str] = None,
            set_collection: Optional[int] = None,
            limit_to: Optional[PosixPath] = None,
            callback_url: Optional[str] = None
    ) -> models.Action:
        """
        Request a handover to labid of a data item.
        """
        payload = {
            ('copyTo', copy_to),
            ('comment', comment),
            ('deleteOriginal', delete_original),
            ('setName', set_name) if set_name else None,
            ('setCollection', set_collection) if set_collection else None,
            ('limitTo', limit_to) if limit_to else None,
            ('callbackUrl', callback_url) if callback_url else None,
        }
        resp = self.session.post(
            f'/data/{data_id}/handover/labid',
            json=dict(filter(bool, payload))  # type: ignore
        )
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to request handover to LabID: HTTP{ resp.status_code } "{ resp.text}".'
            )
        return models.Action(**resp.json())

    @login_required
    def request_handover(  # pylint:disable=R0913
        self,
        data_id: int,
        copy_to: PosixPath,
        set_owner: str,
        comment: str,
        delete_original: bool,
        set_name: Optional[str] = None,
        set_collection: Optional[int] = None,
        limit_to: Optional[PosixPath] = None,
        callback_url: Optional[str] = None
    ) -> models.Action:
        """
        Request a handover of a data item.
        """
        payload = {
            ('copyTo', copy_to),
            ('setOwner', set_owner),
            ('comment', comment),
            ('deleteOriginal', delete_original),
            ('setName', set_name) if set_name else None,
            ('setCollection', set_collection) if set_collection else None,
            ('limitTo', limit_to) if limit_to else None,
            ('callbackUrl', callback_url) if callback_url else None,
        }
        resp = self.session.post(
            f'/data/{data_id}/handover',
            json=dict(filter(bool, payload))  # type: ignore
        )
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to request handover: HTTP{ resp.status_code } "{ resp.text}".'
            )
        return models.Action(**resp.json())

    @login_required
    def list_volumes(self) -> List[models.Volume]:
        """
        Get the volumes for the current user.
        """
        resp = self.session.get('/user/volumes')  # type: ignore
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to get user volumes: HTTP{resp.status_code} "{resp.text}".'
            )
        return [models.Volume(**kw) for kw in resp.json()]

    @login_required
    def list_remote_machines(self) -> List[models.Machine]:
        """
        Get a list of all remote machines which are currently configured for data pull / pullover.
        """
        resp = self.session.get('/machines/remote')
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to get remote machines: HTTP{resp.status_code} "{resp.text}".'
            )
        return [models.Machine(**kw) for kw in resp.json()]

    @login_required
    def list_volumes_for_remote_machine(self, machine_id: int) -> List[models.Volume]:
        """
        Get a list of all volumes configured for the specified remote machine.
        """
        resp = self.session.get(f'/machines/remote/{machine_id}/volumes',)
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to get volumes for remote machines: HTTP{resp.status_code} "{resp.text}".'
            )
        return [models.Volume(**kw) for kw in resp.json()]

    @login_required
    def post(self, path: str, payload: dict) -> requests.Response:
        """
        Generic POST to the given path.
        """
        resp = self.session.post(path, json=payload)
        if resp.status_code != 200:
            raise ClientActionError(
                f'Failed to handle POST request: HTTP{ resp.status_code } "{ resp.text }".'
            )
        return resp
