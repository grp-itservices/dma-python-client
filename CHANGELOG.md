Product change log
==================

### 0.0.33
Add `get_metadata` and `delete_metadata` functions to client. 

### 0.0.32
Extend `Machine` model to include `label`, `note` and `responsible_person` fields.

### 0.0.31
Add `list_remote_machines` and `list_volumes_for_remote_machine` functions to client.

### 0.0.30
Add `get_collection` and `list_collections` functions to client.

### 0.0.29
Add `action_result` to Action model.

### 0.0.27
Add callback parameter for action request methods. 

### 0.0.26
[Rename](https://git.embl.de/grp-itservices/dma-python-client/-/issues/15) Stocks data registration method to `register_labid_data`.

### 0.0.25
Add user volumes request.

### 0.0.24
Add Stocks data registration request.

### 0.0.23
Add data pull and pullover request.

### 0.0.22
Set Collection ID as an [optional field](https://git.embl.de/grp-itservices/dma-python-client/-/issues/14) in the Data model.

### 0.0.21
Added [Share](https://git.embl.de/grp-itservices/dma-python-client/-/issues/13) request.

### 0.0.20
Added Get [Data Status](https://git.embl.de/grp-itservices/dma-python-client/-/issues/12).

### 0.0.19
Added Get Data Info Request.

### 0.0.18
Added Delete Request.

### 0.0.17
Added [optional `addition_info`](https://git.embl.de/grp-itservices/dma-python-client/-/issues/11) parameter for `list_data` method.

### 0.0.16
* Added generic `post()` method;
* Added de-registration of data and collections;
* Updated documentation.

### 0.0.15
Added possibility to directly set authentication token(s).

### 0.0.14
Added [optional filter parameters](https://git.embl.de/grp-itservices/dma-python-client/-/issues/7) for 
the `list_actions` method.

### 0.0.13
Cache [authenticated session](https://git.embl.de/grp-itservices/dma-python-client/-/issues/5).

### 0.0.12
Reduced minimum Python version requirement to version 3.6.8

### 0.0.11
Added optional `restore_to` parameter for restore requests.

### 0.0.10
Added optional filter parameters for `list_data` endpoint.

### 0.0.9
Added option `application` parameter for [login](https://git.embl.de/grp-itservices/dma-python-client/-/issues/4)

### 0.0.8
* Added optional scope parameter for login
* Added Handover request.

### 0.0.7
* Use datetime type for all [timestamps](https://git.embl.de/grp-itservices/dma-python-client/-/issues/3).
* Set the requester scope field to optional as it does not exist for older actions in the DMA.

### 0.0.6
* Added [list_actions](https://git.embl.de/grp-itservices/dma-python-client/-/issues/1). functionality. 
* Fix [issue with optional parameters](https://git.embl.de/grp-itservices/dma-python-client/-/issues/2) for data registration. 

### 0.0.5
Simplify Registration and Action creation functions.

### 0.0.4
Expose model classes - Data, Collection, DataRegistration, CollectionCreation, 
ArchiveRequest, RestoreRequest & Action.

### 0.0.3
Added Data Registration, Collection creation, Archive Request and Restore Request.

### 0.0.1
Initial release
