"""
    Various test fixtures.
"""
import pytest
import requests

from dma_client import RESTClient

@pytest.fixture(scope='session')
def api_url():
    """
    Return the base API URL.
    """
    url = 'http://localhost:4010/api/v1'
    try:
        requests.get(url, timeout=5)
    except requests.exceptions.ConnectionError:
        raise AssertionError(f'Unable to connect to the test API at { url }.')
    return url

@pytest.fixture(scope='function')
def api_creds():
    """
    Return the default API credentials.
    """
    return {'username': 'foo', 'password': 'bar'}

@pytest.fixture(scope='function')
def test_client(api_url, tmpdir):
    """
    Return a test instance of the RESTful API client.
    """
    return RESTClient(api_url, cache_path=tmpdir / '.cache')

@pytest.fixture(scope='function')
def auth_test_client(test_client, api_creds):
    """
    Return an authenticated REST client.
    """
    test_client.authenticate(**api_creds)
    return test_client
