"""
    High-level client tests.
"""
from typing import List
import pytest

from dma_client.exceptions import ClientActionError
from dma_client import models


@pytest.mark.parametrize('name,path,comment,data_type', (
    (None, None, None, None),
    ('test', None, None, None),
    (None, '/foo', None, None),
    (None, None, 'test comment', None),
    (None, None, None, 'file'),
))
def test_list_data(auth_test_client, name, path, comment, data_type):
    """
    Test the data list endpoint.
    """
    datas = auth_test_client.list_data(
        name_contains=name,
        path_contains=path,
        comment_contains=comment,
        data_type=data_type
    )
    assert len(datas) > 0
    assert isinstance(datas, list)
    assert all((isinstance(d, models.Data) for d in datas))


def test_get_data(auth_test_client):
    """
    Test the get data endpoint.
    """
    data = auth_test_client.get_data(
        data_id=1234
    )
    assert isinstance(data, models.Data)


def test_get_data_status(auth_test_client):
    """
    Test the get data status endpoint.
    """
    data = auth_test_client.get_data_status(
        data_id=1234
    )
    assert isinstance(data, models.DataStatus)
    assert isinstance(data.data_id, int)
    assert all((isinstance(a, models.Archive) for a in data.archives))
    assert isinstance(data.available, bool)
    assert all((isinstance(a, models.Data) for a in data.children))
    assert all((isinstance(a, models.Copy) for a in data.copies))
    assert isinstance(data.copy_of, models.Data)
    assert isinstance(data.deleted, bool)
    assert isinstance(data.has_pending_actions, bool)
    assert isinstance(data.removable, bool)
    assert all((isinstance(a, models.Share) for a in data.shares))


@pytest.mark.parametrize('entity_id,entity_type,action_type,action_status', (
    (None, None, None, None),
    (123, 'DATA', None, None),
    (None, 'DATA', None, None),
    (None, None, 'archive', None),
    (None, None, None, 'done'),
))
def test_list_actions(auth_test_client, entity_id, entity_type, action_type, action_status):
    """
    Test the action list endpoint.
    """
    actions = auth_test_client.list_actions(entity_id, entity_type, action_type, action_status)
    assert actions
    assert isinstance(actions, list)
    assert all((isinstance(a, models.Action) for a in actions))


def test_get_action(auth_test_client):
    """
    Test the get action endpoint.
    """
    action = auth_test_client.get_action(
        action_id=1234
    )
    assert isinstance(action, models.Action)


@pytest.mark.parametrize('collection_id,comment,name', (
    (None, None, None),
    (1, None, None),
    (1, 'test comment', None),
    (1, None, 'test_name'),
    (1, 'test comment', 'test_name'),
))
def test_register_data(auth_test_client, collection_id, comment, name):
    """
    Test the data registration endpoint.
    """
    resp = auth_test_client.register_data(
        path='/foo/bar',
        collection_id=collection_id,
        comment=comment,
        name=name,
    )
    assert isinstance(resp, models.Data)
    assert resp is not None


@pytest.mark.parametrize('collection_id,comment,name', (
    (None, None, None),
    (1, None, None),
    (1, 'test comment', None),
    (1, None, 'test_name'),
    (1, 'test comment', 'test_name'),
))
def test_register_labid_data(auth_test_client, collection_id, comment, name):
    """
    Test the labid data registration endpoint.
    """
    resp = auth_test_client.register_labid_data(
        path='/g/test/stocks/bar',
        collection_id=collection_id,
        comment=comment,
        name=name,
    )
    assert isinstance(resp, models.Data)
    assert resp is not None


@pytest.mark.parametrize('name_contains,description_contains', (
    (None, None),
    ('test_name', None),
    (None, 'test'),
))
def test_list_collections(auth_test_client, name_contains, description_contains):
    """
    Test the collection list endpoint.
    """
    collections = auth_test_client.list_collections(
        name_contains=name_contains,
        description_contains=description_contains
    )
    assert len(collections) > 0
    assert isinstance(collections, list)
    assert all((isinstance(c, models.Collection) for c in collections))


def test_get_collection(auth_test_client):
    """
    Test the get collection endpoint.
    """
    data = auth_test_client.get_collection(
        collection_id=1234
    )
    assert isinstance(data, models.Collection)


def test_register_collection(auth_test_client):
    """
    Test the collection registration endpoint.
    """
    resp = auth_test_client.register_collection(
        name='test collection',
        comment='test comment'
    )
    assert isinstance(resp, models.Collection)
    assert resp is not None


def test_archive_request(auth_test_client):
    """
    Test the request archive endpoint
    """
    resp = auth_test_client.request_archive(
        data_id=1234,
        budget_number='12345',
        delete_data=True,
        comment='test comment',
        callback_url="https://foobar.com/callback"
    )
    assert isinstance(resp, models.Action)
    assert resp is not None


def test_delete_request(auth_test_client):
    """
    Test the request delete endpoint
    """
    resp = auth_test_client.request_delete(
        data_id=1234
    )
    assert isinstance(resp, models.Action)
    assert resp is not None


def test_share_request(auth_test_client):
    """
    Test the request share endpoint
    """
    resp = auth_test_client.request_share(
        data_id=1234,
        share_with='userFoo',
        write_access=True,
        comment='test comment',
        callback_url="https://foobar.com/callback"
    )
    assert isinstance(resp, models.Action)
    assert resp is not None

@pytest.mark.parametrize('archive_id,restore_to', (
    (1, None),
    (2, '/foo/bar'),
))
def test_restore_request(auth_test_client, archive_id, restore_to):
    """
    Test the request restore endpoint.
    """
    resp = auth_test_client.request_restore(
        data_id=123,
        archive_id=archive_id,
        restore_to=restore_to,
    )
    assert isinstance(resp, models.Action)
    assert resp is not None


@pytest.mark.parametrize('set_name,set_collection,delete_original,limit_to', (
    (None, None, False, None),
    ('new_name', None, True, None),
    (None, 999, False, None),
    (None, None, True, '/baz'),
    ('new_name', 999, True, '/baz'),
))
def test_handover(
    auth_test_client,
    set_name,
    set_collection,
    delete_original,
    limit_to,
):
    """
    Test the data handover endpoint.
    """
    resp = auth_test_client.request_handover(
        data_id=123,
        copy_to='/foo/bar',
        set_owner='murphy',
        comment='Testing',
        delete_original=delete_original,
        set_name=set_name,
        set_collection=set_collection,
        limit_to=limit_to,
        callback_url='https://foobar.com/callback'
    )
    assert isinstance(resp, models.Action)
    assert resp is not None



@pytest.mark.parametrize('comment,name', (
    (None, None),
    ('test comment', None),
    (None, 'test_name'),
    ('test comment', 'test_name'),
))
def test_pull(auth_test_client, comment, name):
    """
    Test the data pull endpoint.
    """
    resp = auth_test_client.request_pull(
        target_path='/foo/bar',
        original_path='/remote/machine/path',
        original_machine='fooMachine',
        collection_id=1234,
        comment=comment,
        name=name,
        callback_url='https://foobar.com/callback'
    )
    assert isinstance(resp, models.Data)
    assert resp is not None


@pytest.mark.parametrize('collection_id,comment,name', (
    (None, None, None),
    (1, None, None),
    (1, 'test comment', None),
    (1, None, 'test_name'),
    (1, 'test comment', 'test_name'),
))
def test_pullover(auth_test_client, collection_id, comment, name):
    """
    Test the data pullover endpoint.
    """
    resp = auth_test_client.request_pullover(
        target_path='/foo/bar',
        original_path='/remote/machine/path',
        original_machine='fooMachine',
        set_owner='fooUser',
        collection_id=collection_id,
        comment=comment,
        name=name,
    )
    assert isinstance(resp, models.Data)
    assert resp is not None

def test_list_volumes(auth_test_client):
    """
    Test the volume list endpoint.
    """
    volumes = auth_test_client.list_volumes()
    assert len(volumes) > 0
    assert isinstance(volumes, list)
    assert all((isinstance(v, models.Volume) for v in volumes))

@pytest.mark.skip(reason="Endpoint not available in docs / mock api")
def test_list_remote_machines(auth_test_client):
    """
    Test the machines/remote list endpoint.
    """
    machines = auth_test_client.list_remote_machines()
    assert len(machines) > 0
    assert isinstance(machines, list)
    assert all((isinstance(v, models.Machine) for v in machines))

@pytest.mark.skip(reason="Endpoint not available in docs / mock api")
def test_list_volumes_for_remote_machine(auth_test_client):
    """
    Test the machines/remote/{id}/volume endpoint.
    """
    volumes = auth_test_client.list_volumes_for_remote_machine()
    assert len(volumes) > 0
    assert isinstance(volumes, list)
    assert all((isinstance(v, models.Volume) for v in volumes))

@pytest.mark.parametrize('data_id', [1234])
def test_get_metadata(auth_test_client, data_id):
    """
    Test the get metadata endpoint.
    """
    metadata_list = auth_test_client.get_metadata(data_id)
    assert metadata_list is not None
    assert isinstance(metadata_list, list)
    assert all(
        isinstance(metadata, models.MetaData) for metadata in metadata_list
    )

@pytest.mark.parametrize('data_id, metadata_id', [(1234, 5678)])
def test_delete_metadata(auth_test_client, data_id, metadata_id):
    """
    Test the delete metadata endpoint.
    """
    response = auth_test_client.delete_metadata(data_id, metadata_id)
    assert response is None  # Assuming delete_metadata returns None on success

    # Verify that the metadata no longer exists
    metadata_list = auth_test_client.get_metadata(data_id)
    assert metadata_list is not None
    assert metadata_id not in [metadata.id for metadata in metadata_list]
