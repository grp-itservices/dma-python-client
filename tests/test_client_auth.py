"""
    Client authentication tests.
"""
import json

import pytest
import requests
import sqlitedict  # type: ignore

from dma_client import RESTClient, ClientSessionError, ClientConfigError

# FIXME: test unsccessful authentications!

def test_client_cache_created(api_url, tmpdir):
    """
    Make sure the client cache is initialized properly.
    """
    client = RESTClient(api_url)
    assert isinstance(client.cache, dict)
    assert isinstance(client.cache, dict)

    cache_path = tmpdir / 'cache'
    assert not cache_path.exists()
    client = RESTClient(api_url, cache_path)
    assert isinstance(client.cache, sqlitedict.SqliteDict)
    assert cache_path.exists()

    assert 'foo' not in client.cache
    client.cache['foo'] = 'bar'
    assert client.cache['foo'] == 'bar'

def test_authenticate_success(api_url, api_creds):
    """
    Perform a successful authentication.
    """
    client = RESTClient(api_url)
    with pytest.raises(ClientSessionError):
        # pylint:disable=pointless-statement
        client.session
    client.authenticate(**api_creds)
    assert isinstance(client.session, requests.Session)
    assert client.session in client.cache.values()

def test_load_cached_session(api_url, api_creds, tmpdir):
    """
    Cache and reuse a client session.
    """
    client = RESTClient(api_url, cache_path=tmpdir / 'cache')
    with pytest.raises(ClientSessionError):
        # pylint:disable=pointless-statement
        client.session
    client.authenticate(**api_creds)
    assert client.session

    client = RESTClient('http://localhost', cache_path=tmpdir / 'cache')
    with pytest.raises(ClientSessionError):
        # pylint:disable=pointless-statement
        client.session
    client.set_url(api_url)
    assert client.session

def test_client_logout(api_url, api_creds, tmpdir):
    """
    Make sure no session is cached after logging out.
    """
    client = RESTClient(api_url, cache_path=tmpdir / 'cache')
    client.authenticate(**api_creds)
    assert client.session  # pylint:disable=pointless-statement
    client.logout()
    with pytest.raises(ClientSessionError):
        # pylint:disable=pointless-statement
        client.session
    with pytest.raises(ClientSessionError):
        client.logout()

def test_set_authentication_token(api_url):
    """
    Set authentication token.
    """
    client = RESTClient(api_url)
    with pytest.raises(ClientSessionError):
        # pylint:disable=pointless-statement
        client.session
    client.set_auth_token('some_auth_token')
    assert isinstance(client.session, requests.Session)
    assert client.session in client.cache.values()

def test_bulk_load_tokens(tmpdir):
    """
    Bulk load authentication tokens.
    """
    conf = json.dumps({'auth_tokens': [{'url': 'http://foo', 'token': 'footoken'}]})
    path = tmpdir / 'config.json'
    path.write_text(conf, encoding='utf8')
    client = RESTClient('http://fooapi', config_path=path)
    key = 'session:http://foo'
    assert key in client.cache, 'Session has not been cached.'
    session = client.cache[key]
    # pylint:disable=protected-access
    assert session._bearer_token == 'footoken', 'Session token has not been set.'

@pytest.mark.parametrize('json_str', (
    'invalid JSON',
    '["invalid root element"]',
    '{"auth_tokens": "of a wrong type"}',
    '{"auth_tokens": [{"url": "foo"}]}',
    '{"auth_tokens": [{"token": "bar"}]}',
))
def test_bulk_load_tokens_error(tmpdir, json_str):
    """
    Attempt to bulk load a malformed token configuration.
    """
    path = tmpdir / 'config.json'
    with pytest.raises(ClientConfigError):
        RESTClient('http://barapi', config_path=path)

    path.write_text(json_str, encoding='utf8')
    with pytest.raises(ClientConfigError):
        RESTClient('http://barapi', config_path=str(path))
